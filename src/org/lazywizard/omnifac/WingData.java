package org.lazywizard.omnifac;

import com.fs.starfarer.api.campaign.CargoStackAPI;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.loading.FighterWingSpecAPI;

import java.util.HashMap;


class WingData extends ShipyardData {
    int tier;

    WingData(Shipyard shipyard, CargoStackAPI stack) {
        super(shipyard);
        ordered = 0;
        cost = new HashMap<>();
        FighterWingSpecAPI spec = stack.getFighterWingSpecIfWing();
        id = (String) stack.getData();
        displayName = stack.getDisplayName();
        tier = spec.getTier() + 1;
        size = stack.getCargoSpacePerUnit() * 10;
        //1 10
        stackSize = (int) (50 / size);
        lastUpdate = shipyard.getNumHeartbeats();
        baseCost = (int) spec.getBaseValue();
        cost.put(Commodities.RARE_METALS, (int) (size * spec.getTier()));
    }

    @Override
    public String getDisplayType() {
        return "wing";
    }

    @Override
    public int getDaysToAnalyze() {
        return (int) Math.max(1f,
                getBaseDays() * OmniFacSettings.getWingAnalysisTimeMod());
    }

    @Override
    protected int getBaseDays() {
        return (int) (size * tier);
    }

    @Override
    public int getDaysToCreate() {
        return (int) Math.max(getBaseDays() * OmniFacSettings.getWingProductionTimeMod(), 1f);
    }

//        @Override
//        public int getTotal()
//        {
//            return getStorageCargo().getNumFighters(id) - goal;
//        }

    @Override
    public int getLimit() {
        return (int) (stackSize * OmniFacSettings.getMaxStacksPerWing());
    }

    @Override
    public boolean create(boolean really) {
        lastUpdate = shipyard.getNumHeartbeats();

        if (getTotal() >= getLimit()) {
            return false;
        }

        warnedLimit = false;
        if (really) {
            shipyard.getStorageCargo().addFighters(id, 1);
            ordered--;
        } else {
            if (OmniFacSettings.shouldReturnAnalyzedWing()) {
                shipyard.getStorageCargo().addFighters(id, 1);
            }
            shipyard.getFactoryCargo().addFighters(id, getLimit() - shipyard.getFactoryCargo().getNumFighters(id));
        }
        return true;
    }
}
