package org.lazywizard.omnifac;

import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipHullSpecAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.loading.WeaponSlotAPI;

import java.util.HashMap;


class ShipData extends ShipyardData {
    String shipName;
    FleetMemberType type;
    int fp, supplies;

    ShipData(Shipyard shipyard, FleetMemberAPI ship) {
        super(shipyard);
        ordered = 0;
        goal = 0;
        String rawid = ship.getHullId();
        id = rawid + (rawid.contains("default_D") ? "" : "_Hull");
//            id = ship.getHullId() + "_Hull";
        displayName = OmniFacUtil.parseHullName(ship);
        shipName = ship.getShipName();
        type = ship.getType();
        fp = ship.getFleetPointCost();
        ShipHullSpecAPI spec = ship.getHullSpec();
        size = spec.getHullSize().ordinal();
        supplies = (int) ship.getBaseDeploymentCostSupplies();
        lastUpdate = shipyard.getNumHeartbeats();
        baseCost = (int) spec.getBaseValue();
        cost = getShipCost(ship, spec);
    }

    private int getBaseHitpoints(ShipAPI.HullSize hullSize) {
        switch (hullSize) {
            case FRIGATE:
                return 500;
            case DESTROYER:
                return 1000;
            case CRUISER:
                return 2000;
            case CAPITAL_SHIP:
                return 3500;
            default:
                return 100;
        }
    }

    private int getWeaponSlotPoints(WeaponSlotAPI slot) {
        switch (slot.getSlotSize()) {
            case SMALL:
                return 5;
            case MEDIUM:
                return 10;
            case LARGE:
                return 20;
            default:
                return 0;
        }
    }

    private int getAllWeaponSlotsPoints(ShipHullSpecAPI spec) {
        int points = 0;
        for (WeaponSlotAPI slot : spec.getAllWeaponSlotsCopy()) {
            points += getWeaponSlotPoints(slot);
        }
        return points;
    }

    private int getFreeWeaponSlotsPoints(ShipHullSpecAPI spec) {
        int points = 0;
        for (WeaponSlotAPI slot : spec.getAllWeaponSlotsCopy()) {
            if (!slot.isBuiltIn()) {
                points += getWeaponSlotPoints(slot);
            }
        }
        return points;
    }

    private HashMap<String, Integer> getShipCost(FleetMemberAPI ship, ShipHullSpecAPI spec) {
        int weaponPoints = getAllWeaponSlotsPoints(spec);
        int freeWeaponPoints = getFreeWeaponSlotsPoints(spec);
        int hangarPoints = (spec.getFighterBays() - spec.getBuiltInWings().size()) * 10;
        int unusedOrdnancePoints = Math.max(0, spec.getOrdnancePoints(null) - freeWeaponPoints - hangarPoints);

        int baseHitpoints = getBaseHitpoints(spec.getHullSize());
        float hullVolume = baseHitpoints + spec.getHitpoints() + spec.getFighterBays() * 20 * 20 + weaponPoints * 20 + unusedOrdnancePoints * 10;
        float mass = hullVolume * (200.0f + spec.getArmorRating()) / 8000.f;
        float maxSpeed = ship.getStats().getMaxSpeed().getBaseValue();
        float enginePower = mass * (10.f + maxSpeed) / 1000.f;

        float shieldQuality = (
            spec.getShieldSpec().getFluxPerDamageAbsorbed() != 0 ?
                Math.max(1.f, 1.f / spec.getShieldSpec().getFluxPerDamageAbsorbed()) :
                1.f
        );

        HashMap<String, Integer> cost = new HashMap<>();
        cost.put(Commodities.METALS, Math.round(mass));
        cost.put(Commodities.HEAVY_MACHINERY, Math.round(
            spec.getFighterBays() * 20 +
                weaponPoints * 0.5f +
                unusedOrdnancePoints * 0.25f +
                spec.getCargo() / 20.f
        ));
        cost.put(Commodities.RARE_METALS, Math.round(
            spec.getFighterBays() * 5.f +
                weaponPoints * 0.2f +
                unusedOrdnancePoints * 0.2f +
                spec.getFluxCapacity() * shieldQuality / 1000 +
                spec.getFluxDissipation() / 50 +
                spec.getShieldSpec().getPhaseUpkeep() / 5 +
                enginePower / 2.f +
                spec.getFuel() / 50.f
        ));
        cost.put(Commodities.ORGANICS, Math.round(
            spec.getMaxCrew() / 5.0f)
        );
        cost.put(Commodities.VOLATILES, Math.round(
            spec.getMaxCrew() / 5.0f +
                spec.getFighterBays() * 20 +
                spec.getFluxCapacity() * shieldQuality / 200 +
                spec.getFluxDissipation() / 10 +
                spec.getShieldSpec().getUpkeepCost() / 10 +
                spec.getShieldSpec().getPhaseUpkeep() / 1 +
                enginePower
        ));
        return cost;
    }

    @Override
    public String getDisplayType() {
        return "ship";
    }

    @Override
    public int getDaysToAnalyze() {
        return (int) Math.max(1f,
            getBaseDays() * OmniFacSettings.getShipAnalysisTimeMod());
    }

    @Override
    protected int getBaseDays() {
        return fp + supplies;
    }

    @Override
    public int getDaysToCreate() {
        return (int) Math.max(1f,
            getBaseDays() * OmniFacSettings.getShipProductionTimeMod());
    }

//        @Override
//        public int getTotal()
//        {
//            int total = 0;
//
//            for (FleetMemberAPI tmp : getStorageCargo().getMothballedShips().getMembersListCopy())
//                if (id.equals(parseHullName(tmp)))
//                    total++;
//
//            return total;
//        }

    @Override
    public int getLimit() {
        return 1;
    }

    @Override
    public boolean create(boolean really) throws NullPointerException {
        lastUpdate = shipyard.getNumHeartbeats();

        if (getTotal() >= getLimit()) {
            return false;
        }

        warnedLimit = false;
        if (really) {
            shipyard.getStorageCargo().addMothballedShip(type, id, null);
            ordered--;
        } else {
            if (OmniFacSettings.shouldReturnAnalyzedShip()) {
                shipyard.getStorageCargo().addMothballedShip(type, id, shipName);
            }
            shipyard.getFactoryCargo().addMothballedShip(type, id, null);
        }
        return true;
    }
}
