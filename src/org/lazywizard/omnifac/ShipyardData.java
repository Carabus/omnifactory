package org.lazywizard.omnifac;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;

import java.util.HashMap;
import java.util.Map;


public abstract class ShipyardData {
    final Shipyard shipyard;
    String id, displayName;
    float size;
    int ordered, lastUpdate, stackSize, goal;
    boolean warnedLimit = false, isAnalyzed = false;
    int baseCost;
    HashMap<String, Integer> cost;

    public ShipyardData(Shipyard shipyard) {
        this.shipyard = shipyard;
    }

    public abstract String getDisplayType();

    public abstract int getDaysToAnalyze();

    protected abstract int getBaseDays();

    public abstract int getDaysToCreate();

    public int getLastUpdate() {
        return lastUpdate;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getId() {
        return id;
    }

    public int getTotal() {
        return goal - ordered;
    }

    public abstract int getLimit();

    public boolean inProduction() {
        return ordered > 0;
    }

    public int getProduction() {
        if (ordered <= 0)
            goal = 0;
        return ordered;
    }

    public void startProduction(int requested) {
        boolean canAfford = true;
        for (Map.Entry<String, Integer> commodity : cost.entrySet())
            canAfford = canAfford && shipyard.getStorageCargo().getQuantity(CargoAPI.CargoItemType.RESOURCES, commodity.getKey()) >= commodity.getValue() * requested;
        if (canAfford) {
            removeMaterials(requested);
            goal = ordered = requested;
            lastUpdate = shipyard.getNumHeartbeats();
        } else {
            Global.getSector().getCampaignUI().addMessage("Insufficient resources, order cancelled.");
        }
    }

    public int getBaseCost() {
        return baseCost;
    }

    public HashMap<String, Integer> getCost() {
        return cost;
    }

    public boolean hasWarnedLimit() {
        return warnedLimit;
    }

    public void setWarnedLimit(boolean hasWarned) {
        warnedLimit = hasWarned;
    }

    public boolean isAnalyzed() {
        return isAnalyzed;
    }

    public void setAnalyzed(boolean isAnalyzed) {
        this.isAnalyzed = isAnalyzed;
        lastUpdate = shipyard.getNumHeartbeats();
    }

    public void removeMaterials(int mult) {
        for (Map.Entry<String, Integer> commodity : cost.entrySet()) {
            if (commodity.getValue() * mult > 0)
                Global.getSector().getCampaignUI().addMessage("Consumed " + commodity.getValue() * OmniFacSettings.getCostMult() * mult + " " + Global.getSector().getEconomy().getCommoditySpec(commodity.getKey()).getName());
            shipyard.getStorageCargo().removeCommodity(commodity.getKey(), commodity.getValue() * OmniFacSettings.getCostMult() * mult);
        }
    }

    public abstract boolean create(boolean really) throws NullPointerException;
}
