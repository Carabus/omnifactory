package org.lazywizard.omnifac;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.util.Pair;
import org.jetbrains.annotations.Nullable;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.campaign.CargoUtils;
import org.lazywizard.lazylib.campaign.MessageUtils;

import java.util.*;


/**
 * Created by Isaac on 7/22/17.
 */
public final class OmniFac extends Shipyard {
    @Nullable
    private Pair<String, Integer> coreTierReq(int tier) {
        switch (tier) {
            case 0:
            case 1:
                return OmniFacSettings.getCores(1);
            case 2:
                return OmniFacSettings.getCores(2);
            case 3:
                return OmniFacSettings.getCores(3);
            case 4:
            case 5:
                return OmniFacSettings.getCores(4);
            default:
                return null;
        }
    }

    @Nullable
    private Pair<String, Integer> coreSizeReq(ShipAPI.HullSize hullSize) {
        switch (hullSize) {
            case FIGHTER:
                return null;
            case FRIGATE:
                return OmniFacSettings.getCores(1);
            case DESTROYER:
                return OmniFacSettings.getCores(2);
            case CRUISER:
                return OmniFacSettings.getCores(3);
            case CAPITAL_SHIP:
                return OmniFacSettings.getCores(4);
            case DEFAULT:
            default:
                return null;
        }
    }

    @Nullable
    private String coreTier(int tier) {
        switch (tier) {
            case 0:
            case 1:
                return getStorageCargo().getQuantity(CargoAPI.CargoItemType.RESOURCES, OmniFacSettings.getCores(1).one) < OmniFacSettings.getCores(1).two ? GAMMA_CORE_NEEDED : null;
            case 2:
                return getStorageCargo().getQuantity(CargoAPI.CargoItemType.RESOURCES, OmniFacSettings.getCores(2).one) < OmniFacSettings.getCores(2).two ? GAMMA_CORES_NEEDED : null;
            case 3:
                return getStorageCargo().getQuantity(CargoAPI.CargoItemType.RESOURCES, OmniFacSettings.getCores(3).one) < OmniFacSettings.getCores(3).two ? BETA_CORE_NEEDED : null;
            case 4:
                return getStorageCargo().getQuantity(CargoAPI.CargoItemType.RESOURCES, OmniFacSettings.getCores(4).one) < OmniFacSettings.getCores(4).two ? ALPHA_CORE_NEEDED : null;
            default:
                return null;
        }
    }

    @Nullable
    private String coreSize(ShipAPI.HullSize hullSize) {
        switch (hullSize) {
            case FIGHTER:
                return null;
            case FRIGATE:
                return getStorageCargo().getQuantity(CargoAPI.CargoItemType.RESOURCES, OmniFacSettings.getCores(1).one) < OmniFacSettings.getCores(1).two ? GAMMA_CORE_NEEDED : null;
            case DESTROYER:
                return getStorageCargo().getQuantity(CargoAPI.CargoItemType.RESOURCES, OmniFacSettings.getCores(2).one) < OmniFacSettings.getCores(2).two ? GAMMA_CORES_NEEDED : null;
            case CRUISER:
                return getStorageCargo().getQuantity(CargoAPI.CargoItemType.RESOURCES, OmniFacSettings.getCores(3).one) < OmniFacSettings.getCores(3).two ? BETA_CORE_NEEDED : null;
            case CAPITAL_SHIP:
                return getStorageCargo().getQuantity(CargoAPI.CargoItemType.RESOURCES, OmniFacSettings.getCores(4).one) < OmniFacSettings.getCores(4).two ? ALPHA_CORE_NEEDED : null;
            case DEFAULT:
            default:
                return null;
        }
    }

    public boolean isUnknownShip(FleetMemberAPI ship) {
        return !getShipData().containsKey(OmniFacUtil.parseHullName(ship));
    }

    public boolean isUnknownWeapon(CargoStackAPI stack) {
        // We only deal with weapons, not resources
        return (stack.isWeaponStack() && !getWepData().containsKey(stack.getData()));
    }

    public boolean isUnknownWing(CargoStackAPI stack) {
        // We only deal with wings, not resources
        return (stack.isFighterWingStack() && !getWingData().containsKey(stack.getData()));
    }

    private void checkCargo() {
        final CargoAPI cargo = getFactoryCargo(), storage = getStorageCargo();
        final List<String> newShips = new ArrayList<>(), blockedShips = new ArrayList<>(),
            newWeps = new ArrayList<>(), blockedWeps = new ArrayList<>(),
            newWings = new ArrayList<>(), blockedWings = new ArrayList<>();

        for (FleetMemberAPI ship : cargo.getMothballedShips().getMembersListCopy()) {
            if (OmniFacUtil.isRestrictedShip(ship)) {
                blockedShips.add(ship.getHullSpec().getHullName());
                cargo.getMothballedShips().removeFleetMember(ship);
                storage.getMothballedShips().addFleetMember(ship);
            } else if (isUnknownShip(ship)) {
                String id = OmniFacUtil.parseHullName(ship);
                ShipData tmp = new ShipData(this, ship);
                Pair<String, Integer> cores = coreSizeReq(ship.getHullSpec().getHullSize());
                storage.removeCommodity(cores.one, cores.two);

                if (OmniFacSettings.getShipAnalysisTimeMod() == 0f) {
                    utilCreate(tmp, false);
                    tmp.setAnalyzed(true);
                    newShips.add(tmp.getDisplayName() + " ("
                        + tmp.getDaysToCreate() + "d)");
                } else
                    newShips.add(tmp.getDisplayName() + " ("
                        + tmp.getDaysToAnalyze() + "d)");

                getShipData().put(id, tmp);

                // Add all weapons on this ship to the station's storage
                for (String slot : ship.getVariant().getNonBuiltInWeaponSlots())
                    storage.addWeapons(ship.getVariant().getWeaponId(slot), 1);
                for (String wingId : ship.getVariant().getWings())
                    if (wingId.length() > 0)
                        storage.addFighters(wingId, 1);

                cargo.getMothballedShips().removeFleetMember(ship);
            }
        }

        for (CargoStackAPI stack : cargo.getStacksCopy()) {
            if (OmniFacUtil.isRestrictedWeapon(stack)) {
                blockedWeps.add(stack.getDisplayName());
                CargoUtils.moveStack(stack, storage);
            } else if (isUnknownWeapon(stack)) {
                WeaponData tmp = new WeaponData(this, stack);
                Pair<String, Integer> cores = coreTierReq(stack.getWeaponSpecIfWeapon().getTier());
                storage.removeCommodity(cores.one, cores.two);

                if (OmniFacSettings.getWeaponAnalysisTimeMod() == 0f) {
                    utilCreate(tmp, false);
                    tmp.setAnalyzed(true);
                    newWeps.add(tmp.getDisplayName() + " ("
                        + tmp.getDaysToCreate() + "d)");
                } else
                    newWeps.add(tmp.getDisplayName() + " ("
                        + tmp.getDaysToAnalyze() + "d)");

                getWepData().put((String) stack.getData(), tmp);
                cargo.removeWeapons((String) stack.getData(), (int) stack.getSize());
            }

            if (OmniFacUtil.isRestrictedWing(stack)) {
                blockedWings.add(stack.getDisplayName());
                CargoUtils.moveStack(stack, storage);
            } else if (isUnknownWing(stack)) {
                WingData tmp = new WingData(this, stack);
                Pair<String, Integer> cores = coreTierReq(stack.getFighterWingSpecIfWing().getTier());
                storage.removeCommodity(cores.one, cores.two);

                if (OmniFacSettings.getWingAnalysisTimeMod() == 0f) {
                    utilCreate(tmp, false);
                    tmp.setAnalyzed(true);
                    newWings.add(tmp.getDisplayName() + " ("
                        + tmp.getDaysToCreate() + "d)");
                } else {
                    newWings.add(tmp.getDisplayName() + " ("
                        + tmp.getDaysToAnalyze() + "d)");
                }

                getWingData().put((String) stack.getData(), tmp);
                cargo.removeItems(CargoAPI.CargoItemType.FIGHTER_CHIP, stack.getData(), stack.getSize());
            }
        }

        if (!newShips.isEmpty())
            addBluePrint(newShips, SHIP_NAME);
        if (!newWeps.isEmpty())
            addBluePrint(newWeps, WEAPON_NAME);
        if (!newWings.isEmpty())
            addBluePrint(newWings, WING_NAME);

        boolean movedItems = false;
        if (!blockedShips.isEmpty())
            movedItems = blockBluePrint(blockedShips, SHIP_NAME);
        if (!blockedWeps.isEmpty())
            movedItems = blockBluePrint(blockedWeps, WEAPON_NAME);
        if (!blockedWings.isEmpty())
            movedItems = blockBluePrint(blockedWings, WEAPON_NAME);

        if (movedItems)
            Global.getSector().getCampaignUI().addMessage("All non-replicable"
                + " items have been moved to " + getStation().getName() + "'s storage.");
    }

    @Override
    protected void addBluePrint(List<String> newItems, String name) {
        Collections.sort(newItems);
        if (OmniFacSettings.getShipAnalysisTimeMod() == 0f) {
            MessageUtils.showMessage("New " + name + " blueprints added to the "
                    + getStation().getName() + ":",
                CollectionUtils.implode(newItems) + ".", true);
        } else {
            MessageUtils.showMessage("The following " + name + "s are being"
                    + " disassembled and analyzed by the "
                    + getStation().getName() + ":",
                CollectionUtils.implode(newItems) + ".", true);
        }
    }

    @Override
    protected boolean blockBluePrint(List<String> blocked, String name) {
        Collections.sort(blocked);
        MessageUtils.showMessage("The " + getStation().getName()
                + " is unable to replicate the following " + name + "s:",
            CollectionUtils.implode(blocked) + ".", true);
        return true;
    }

    @Override
    protected void orderedMessage(List<String> ordered, String name) {
        Collections.sort(ordered);
        MessageUtils.showMessage("The " + getStation().getName()
                + " has begun production on the following " + name + "s:",
            CollectionUtils.implode(ordered) + ".", true);
    }

    @Override
    protected void producedMessage(List<String> added, String name) {
        Collections.sort(added);
        MessageUtils.showMessage("The " + getStation().getName()
                + " has produced the following " + name + "s:",
            CollectionUtils.implode(added) + ".", true);
    }

    @Override
    protected void finishMessage(List<String> analyzed, String name) {
        Collections.sort(analyzed);
        MessageUtils.showMessage("The " + getStation().getName()
                + " has finished analyzing the following " + name + "s:",
            CollectionUtils.implode(analyzed) + ".", true);
    }

    @Override
    protected void processBlueprint(ShipyardData tmp, List<String> analyzed, List<String> added, List<String> limit, String name) {
        if (!tmp.isAnalyzed())
            if (checkTimerAnalyze(tmp)) {
                try {
                    utilCreate(tmp, false);
                } catch (RuntimeException e) {
                    getShipData().remove(tmp.getDisplayName());
                }
                tmp.setAnalyzed(true);

                if (OmniFacSettings.shouldShowAnalysisComplete())
                    analyzed.add(tmp.getDisplayName() + " ("
                        + tmp.getDaysToCreate() + "d)");
            }
        if (tmp.inProduction() && (checkTimerCreate(tmp)))
            try {
                Warning(tmp, added, limit);
            } catch (RuntimeException ex) {
                Global.getSector().getCampaignUI().addMessage(
                    "Failed to create " + name + " '" + tmp.getDisplayName() + "' ("
                        + tmp.getId() + ")! Was a required mod disabled?");

                if (OmniFacSettings.shouldRemoveBrokenGoods()) {
                    Global.getSector().getCampaignUI().addMessage(
                        "Removed " + name + " '" + tmp.getDisplayName() + "' from "
                            + getStation().getName() + "'s memory banks.");
                    getShipData().remove(tmp.getDisplayName());
                }
            }
    }

    @Override
    public void reportPlayerMarketTransaction(PlayerMarketTransaction transaction) {
        List<String> orderedShips = new ArrayList<>(), orderedWeapons = new ArrayList<>(), orderedWings = new ArrayList<>();

        for (PlayerMarketTransaction.ShipSaleInfo saleInfo : transaction.getShipsBought()) {
            ShipyardData tmp;
            FleetMemberAPI ship = saleInfo.getMember();
            if (!isUnknownShip(ship))
                tmp = getShipData().get(OmniFacUtil.parseHullName(ship));
            else
                continue;
            if (!tmp.inProduction()) {
                orderedShips.add(tmp.getDisplayName());
                tmp.startProduction(1);
            }
            utilCreate(tmp, false);
            Global.getSector().getPlayerFleet().removeFleetMemberWithDestructionFlash(ship);
        }

        for (CargoStackAPI stack : transaction.getBought().getStacksCopy()) {
            ShipyardData tmp;
            if (stack.isWeaponStack() && !isUnknownWeapon(stack) && !OmniFacUtil.isRestrictedWeapon(stack)) {
                tmp = getWepData().get(stack.getData());
                Global.getSector().getPlayerFleet().getCargo().removeWeapons(tmp.getId(), (int) stack.getSize());
                if (!tmp.inProduction())
                    orderedWeapons.add(tmp.getDisplayName());
            } else if (stack.isFighterWingStack() && !isUnknownWing(stack) && !OmniFacUtil.isRestrictedWing(stack)) {
                tmp = getWingData().get(stack.getData());
                Global.getSector().getPlayerFleet().getCargo().removeItems(CargoAPI.CargoItemType.FIGHTER_CHIP, stack.getData(), stack.getSize());
                if (!tmp.inProduction())
                    orderedWings.add(tmp.getDisplayName());
            } else
                continue;

            if (!tmp.inProduction())
                tmp.startProduction((int) stack.getSize());
            utilCreate(tmp, false);
        }

        if (!orderedShips.isEmpty())
            orderedMessage(orderedShips, SHIP_NAME);
        if (!orderedWeapons.isEmpty())
            orderedMessage(orderedWeapons, WEAPON_NAME);
        if (!orderedWings.isEmpty())
            orderedMessage(orderedWings, WING_NAME);

        Global.getSector().getPlayerFleet().getCargo().getCredits().add((-1 + OmniFacSettings.getCreditCostMult()) * transaction.getCreditValue());
        transaction.setCreditValue(0);
        transaction.getShipsBought().clear();
        transaction.getBought().clear();
        checkCargo();
    }

    @Override
    public boolean isIllegalOnSubmarket(String commodityId, TransferAction action) {
        return true;
    }

    @Override
    public boolean isIllegalOnSubmarket(CargoStackAPI stack, TransferAction action) {
        if (action == TransferAction.PLAYER_SELL) {
            // Can't sell restricted or known weapons to the Omnifactory
            if (OmniFacUtil.isRestrictedWeapon(stack) || OmniFacUtil.isRestrictedWing(stack) || (!isUnknownWeapon(stack) && !isUnknownWing(stack)))
                return true;
            // Require AI cores to analyse.
            if (stack.isWeaponStack())
                return coreTier(stack.getWeaponSpecIfWeapon().getTier()) != null;
            if (stack.isFighterWingStack())
                return coreTier(stack.getFighterWingSpecIfWing().getTier()) != null;
        }

        if (action == TransferAction.PLAYER_BUY) {
            // Prevent buying from refit screen.
            if (isRefit())
                return true;
            // Require the presence of sufficient resources.
            boolean affordable = !isDisplayCosts();
            if (stack.isWeaponStack() && !isUnknownWeapon(stack) && !OmniFacUtil.isRestrictedWeapon(stack))
                for (Map.Entry<String, Integer> commodity : getWepData().get(stack.getData()).getCost().entrySet())
                    affordable &= getStorageCargo().getCommodityQuantity(commodity.getKey()) >= commodity.getValue() * OmniFacSettings.getCostMult();
            if (stack.isFighterWingStack() && !isUnknownWing(stack) && !OmniFacUtil.isRestrictedWing(stack))
                for (Map.Entry<String, Integer> commodity : getWingData().get(stack.getData()).getCost().entrySet())
                    affordable &= getStorageCargo().getCommodityQuantity(commodity.getKey()) >= commodity.getValue() * OmniFacSettings.getCostMult();
            return !affordable;
        }

        return false;
    }

    @Override
    public boolean isIllegalOnSubmarket(FleetMemberAPI ship, TransferAction action) {
        if (action == TransferAction.PLAYER_SELL) {
            // Can't sell restricted or known ships to the Omnifactory
            if (OmniFacUtil.isRestrictedShip(ship) || !isUnknownShip(ship))
                return true;
            return coreSize(ship.getHullSpec().getHullSize()) != null;
        }

        if (action == TransferAction.PLAYER_BUY) {
            // Require the presence of sufficient resources.
            boolean affordable = !isDisplayCosts();
            if (!isUnknownShip(ship) && !OmniFacUtil.isRestrictedShip(ship))
                for (Map.Entry<String, Integer> commodity : getShipData().get(OmniFacUtil.parseHullName(ship)).getCost().entrySet())
                    affordable &= getStorageCargo().getCommodityQuantity(commodity.getKey()) >= commodity.getValue() * OmniFacSettings.getCostMult();
            return !affordable;
        }

        return false;
    }

    @Override
    public String getIllegalTransferText(CargoStackAPI stack, TransferAction action) {
        String ret = "";

        if (isDisplayCosts()) {
            if (stack.isWeaponStack())
                for (Map.Entry<String, Integer> commodity : getWepData().get(stack.getData()).getCost().entrySet())
                    ret.concat("Requires " + commodity.getValue() * OmniFacSettings.getCostMult() + " " + commodity.getKey() + "\n");
            if (stack.isFighterWingStack())
                for (Map.Entry<String, Integer> commodity : getWingData().get(stack.getData()).getCost().entrySet())
                    ret.concat("Requires " + commodity.getValue() * OmniFacSettings.getCostMult() + " " + commodity.getKey() + "\n");
            return ret;
        }

        if (action == TransferAction.PLAYER_BUY)
            return "Insufficient Resources\n";
        if (stack.isWeaponStack())
            return OmniFacUtil.isRestrictedWeapon(stack) ? NO_REPLICATE : (ret = coreTier(stack.getWeaponSpecIfWeapon().getTier())) == null ? HAS_BLUEPRINT : ret;
        if (stack.isFighterWingStack())
            return OmniFacUtil.isRestrictedWing(stack) ? NO_REPLICATE : (ret = coreTier(stack.getFighterWingSpecIfWing().getTier())) == null ? HAS_BLUEPRINT : ret;

        // Omnifactory can only replicate weapons and fighter wings.
        return NO_REPLICATE;
    }

    @Override
    public String getIllegalTransferText(FleetMemberAPI ship, TransferAction action) {
        String ret = "";

        if (isDisplayCosts()) {
            if (!isUnknownShip(ship) && !OmniFacUtil.isRestrictedShip(ship))
                for (Map.Entry<String, Integer> commodity : getShipData().get(OmniFacUtil.parseHullName(ship)).getCost().entrySet())
                    ret.concat("Requires " + commodity.getValue() * OmniFacSettings.getCostMult() + " " + commodity.getKey() + "\n");
            Global.getSector().getCampaignUI().addMessage(ret);
            return ret;
        }

        if (action == TransferAction.PLAYER_BUY)
            return "Insufficient Resources\n";
        return (OmniFacUtil.isRestrictedShip(ship) ? NO_REPLICATE : (ret = coreSize(ship.getHullSpec().getHullSize())) == null ? HAS_BLUEPRINT : ret);
    }
}
