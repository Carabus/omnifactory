package org.lazywizard.omnifac;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.util.Pair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lazywizard.omnifac.secondary.Constants;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


public class OmniFacSettings {
    private static Set<String> restrictedWeapons;
    private static Set<String> restrictedShips;
    private static Set<String> restrictedWings;
    private static Set<String> restrictedMarkets;
    private static String targetPlanet;
    private static String targetSystem;
    private static boolean randomLocation;
    private static boolean randomLocationCivilized;
    private static boolean showAddedCargo;
    private static boolean showAnalysisComplete;
    private static boolean returnAnalyzedShip;
    private static boolean returnAnalyzedWing;
    private static boolean returnAnalyzedWeapon;
    private static boolean showLimitReached;
    private static boolean allowRestrictedGoods;
    private static boolean removeBrokenGoods;
    private static float shipAnalysisTimeMod;
    private static float weaponAnalysisTimeMod;
    private static float wingAnalysisTimeMod;
    private static float shipProductionTimeMod;
    private static float weaponProductionTimeMod;
    private static float wingProductionTimeMod;
    private static int requiredCrew;
    private static float requiredSuppliesPerDay;
    private static float requiredFuelPerDay;
    private static int maxHullsPerFighter;
    private static int maxHullsPerFrigate;
    private static int maxHullsPerDestroyer;
    private static int maxHullsPerCruiser;
    private static int maxHullsPerCapital;
    private static float maxStacksPerWeapon;
    private static float maxStacksPerWing;
    private static int numberOfFactories;
    private static float omnifactoryTariff;
    private static float omnifactoryCosts;
    private static float omnifactoryCredits;
    private static int TIERS = 4;
    private static Pair<String, Integer>[] omnifactoryCores = new Pair[TIERS];

    public static void reloadSettings() throws JSONException, IOException {
        // Base Omnifactory settings
        JSONObject settings = Global.getSettings().loadJSON(Constants.SETTINGS_FILE);
        targetPlanet = settings.getString("targetPlanet");
        targetSystem = settings.getString("targetSystem");
        randomLocation = settings.getBoolean("randomStartingLocation");
        randomLocationCivilized = settings.getBoolean("randomStartingLocationCivilized");
        showAddedCargo = settings.getBoolean("showAddedCargo");
        showAnalysisComplete = settings.getBoolean("showAnalysisComplete");
        returnAnalyzedShip = settings.getBoolean("returnAnalyzedShip");
        returnAnalyzedWing = settings.getBoolean("returnAnalyzedWing");
        returnAnalyzedWeapon = settings.getBoolean("returnAnalyzedWeapon");
        showLimitReached = settings.getBoolean("showLimitReached");
        allowRestrictedGoods = settings.getBoolean("ignoreGoodRestrictions");
        removeBrokenGoods = settings.getBoolean("removeBrokenGoods");
        shipAnalysisTimeMod = (float) settings.getDouble("shipAnalysisTimeMod");
        weaponAnalysisTimeMod = (float) settings.getDouble("weaponAnalysisTimeMod");
        wingAnalysisTimeMod = (float) settings.getDouble("wingAnalysisTimeMod");
        shipProductionTimeMod = (float) settings.getDouble("shipProductionTimeMod");
        weaponProductionTimeMod = (float) settings.getDouble("weaponProductionTimeMod");
        wingProductionTimeMod = (float) settings.getDouble("wingProductionTimeMod");
        requiredCrew = settings.getInt("requiredCrewToFunction");
        requiredSuppliesPerDay = (float) settings.getDouble("requiredSuppliesPerDay");
        requiredFuelPerDay = (float) settings.getDouble("requiredFuelPerDay");
        maxHullsPerFighter = settings.getInt("maxHullsPerFighter");
        maxHullsPerFrigate = settings.getInt("maxHullsPerFrigate");
        maxHullsPerDestroyer = settings.getInt("maxHullsPerDestroyer");
        maxHullsPerCruiser = settings.getInt("maxHullsPerCruiser");
        maxHullsPerCapital = settings.getInt("maxHullsPerCapital");
        maxStacksPerWeapon = (float) settings.getDouble("maxStacksPerWeapon");
        maxStacksPerWing = (float) settings.getDouble("maxStacksPerWing");
        numberOfFactories = settings.getInt("numberOfFactories");
        omnifactoryTariff = (float) settings.getDouble("omnifactoryTariff");
        omnifactoryCosts = (float) settings.getDouble("omnifactoryCosts");
        omnifactoryCredits = (float) settings.getDouble("omnifactoryCredits");

        // Cores
        for (int i = 0; i < TIERS; i++)
            omnifactoryCores[i] = new Pair<>(settings.getString("tier" + i + "Type"), settings.getInt("tier" + i + "Int"));

        // Restricted Weapons
        restrictedWeapons = loadRestrictions("weapon id", Constants.RESTRICTED_WEAPONS_CSV, Constants.MOD_ID);

        // Restricted ships
        restrictedShips = loadRestrictions("hull id", Constants.RESTRICTED_SHIPS_CSV, Constants.MOD_ID);

        // Restricted Wings
        restrictedWings = loadRestrictions("wing id", Constants.RESTRICTED_WINGS_CSV, Constants.MOD_ID);

        // Restricted Markets
        restrictedMarkets = loadRestrictions("market id", Constants.RESTRICTED_MARKETS_CSV, Constants.MOD_ID);
    }

    public static Pair<String, Integer> getCores(int tier) {
        return omnifactoryCores[(tier - 1) % TIERS];
    }

    private static Set<String> loadRestrictions(String idColumn, String file, String modId) throws JSONException, IOException {
        Set<String> restricted = new HashSet<>();
        JSONArray csv = Global.getSettings().getMergedSpreadsheetDataForMod(idColumn,
            file, modId);
        for (int x = 0; x < csv.length(); x++) {
            JSONObject row = csv.getJSONObject(x);
            restricted.add(row.getString(idColumn));
        }
        return restricted;
    }

    public static Set<String> getRestrictedWeapons() {
        if (allowRestrictedGoods)
            return Collections.<String>emptySet();
        return restrictedWeapons;
    }

    public static Set<String> getRestrictedShips() {
        if (allowRestrictedGoods)
            return Collections.<String>emptySet();
        return restrictedShips;
    }

    public static Set<String> getRestrictedWings() {
        if (allowRestrictedGoods)
            return Collections.<String>emptySet();
        return restrictedWings;
    }

    public static String getTargetPlanet() {
        return targetPlanet;
    }

    public static String getTargetSystem() {
        return targetSystem;
    }

    public static Set<String> getRestrictedMarkets() {
        return restrictedMarkets;
    }

    public static boolean shouldHaveRandomStartingLocation() {
        return randomLocation;
    }

    public static boolean shouldHaveRandomStartingLocationCivilized() {
        return randomLocationCivilized;
    }

    public static boolean shouldShowAddedCargo() {
        return showAddedCargo;
    }

    public static boolean shouldShowAnalysisComplete() {
        return showAnalysisComplete;
    }

    public static boolean shouldReturnAnalyzedShip() {
        return returnAnalyzedShip;
    }

    public static boolean shouldReturnAnalyzedWing() {
        return returnAnalyzedWing;
    }

    public static boolean shouldReturnAnalyzedWeapon() {
        return returnAnalyzedWeapon;
    }

    public static boolean shouldShowLimitReached() {
        return showLimitReached;
    }

    public static boolean shouldRemoveBrokenGoods() {
        return removeBrokenGoods;
    }

    public static float getShipAnalysisTimeMod() {
        return shipAnalysisTimeMod;
    }

    public static float getWeaponAnalysisTimeMod() {
        return weaponAnalysisTimeMod;
    }

    public static float getWingAnalysisTimeMod() {
        return wingAnalysisTimeMod;
    }

    public static float getShipProductionTimeMod() {
        return shipProductionTimeMod;
    }

    public static float getWingProductionTimeMod() {
        return wingProductionTimeMod;
    }

    public static float getWeaponProductionTimeMod() {
        return weaponProductionTimeMod;
    }

    public static int getRequiredCrew() {
        return requiredCrew;
    }

    public static float getRequiredSuppliesPerDay() {
        return requiredSuppliesPerDay;
    }

    public static float getRequiredFuelPerDay() {
        return requiredFuelPerDay;
    }

    public static int getMaxHullsPerFighter() {
        return maxHullsPerFighter;
    }

    public static int getMaxHullsPerFrigate() {
        return maxHullsPerFrigate;
    }

    public static int getMaxHullsPerDestroyer() {
        return maxHullsPerDestroyer;
    }

    public static int getMaxHullsPerCruiser() {
        return maxHullsPerCruiser;
    }

    public static int getMaxHullsPerCapital() {
        return maxHullsPerCapital;
    }

    public static float getMaxStacksPerWeapon() {
        return maxStacksPerWeapon;
    }

    public static float getMaxStacksPerWing() {
        return maxStacksPerWing;
    }

    public static int getNumberOfFactories() {
        return (randomLocation ? numberOfFactories : 1);
    }

    public static float getOmnifactoryTariff() {
        return omnifactoryTariff;
    }

    private OmniFacSettings() {
    }

    public static float getCostMult() {
        return omnifactoryCosts;
    }

    public static float getCreditCostMult() {
        return omnifactoryCredits;
    }
}
