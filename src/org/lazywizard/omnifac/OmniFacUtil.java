package org.lazywizard.omnifac;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoStackAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.submarkets.StoragePlugin;
import org.lazywizard.omnifac.secondary.Constants;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Isaac on 7/22/17.
 */
public class OmniFacUtil {
    public static void initOmnifactory(SectorEntityToken factory) {
        // Only one controller script per factory
        if (isFactory(factory))
            throw new RuntimeException(factory.getFullName()
                + " is already an Omnifactory!");

        // Set up market data for the Omnifactory
        MarketAPI market = factory.getMarket();

        if (!market.hasSubmarket(Submarkets.SUBMARKET_STORAGE)) {
            market.addSubmarket(Submarkets.SUBMARKET_STORAGE);
        }

        ((StoragePlugin) market.getSubmarket(Submarkets.SUBMARKET_STORAGE)
            .getPlugin()).setPlayerPaidToUnlock(true);
        market.addSubmarket(Constants.SUBMARKET_ID);
        getFactory(factory).setStation(factory);
    }

    public static boolean isFactory(SectorEntityToken station) {
        return (station.getMarket() != null
            && station.getMarket().hasSubmarket(Constants.SUBMARKET_ID));
    }

    public static OmniFac getFactory(SectorEntityToken station) {
        return (OmniFac) station.getMarket().getSubmarket(Constants.SUBMARKET_ID).getPlugin();
    }

    public static SubmarketAPI getFactoryMarket(SectorEntityToken station) {
        return station.getMarket().getSubmarket(Constants.SUBMARKET_ID);
    }

    public static List<Shipyard> getAllFactories() {
        List<Shipyard> factories = new ArrayList<>();
        for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy()) {
            SectorEntityToken token = market.getPrimaryEntity();
            if (isFactory(token)) {
                factories.add(getFactory(token));
            }
        }

        return factories;
    }

    public static String parseHullName(FleetMemberAPI ship) {
        return ship.getHullSpec().getHullName();
    }

    public static boolean isRestrictedShip(FleetMemberAPI ship) {
        return OmniFacSettings.getRestrictedShips().contains(parseHullName(ship)) || ship.getHullId().contains("default_D");
    }

    public static boolean isRestrictedWeapon(CargoStackAPI stack) {
        return stack.isWeaponStack() && OmniFacSettings.getRestrictedWeapons().contains(stack.getData());
    }

    public static boolean isRestrictedWing(CargoStackAPI stack) {
        return stack.isFighterWingStack() && OmniFacSettings.getRestrictedWings().contains(stack.getData());
    }
}
