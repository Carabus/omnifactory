package org.lazywizard.omnifac.secondary;


public class Constants {
    public static final String MOD_ID = "lw_omnifac";
    public static final String SETTINGS_FILE = "data/config/omnifactory/omnifac_settings.json";
    public static final String RESTRICTED_WEAPONS_CSV = "data/config/omnifactory/restricted_weapons.csv";
    public static final String RESTRICTED_SHIPS_CSV = "data/config/omnifactory/restricted_ships.csv";
    public static final String RESTRICTED_WINGS_CSV = "data/config/omnifactory/restricted_wings.csv";
    public static final String RESTRICTED_MARKETS_CSV = "data/config/omnifactory/restricted_markets.csv";
    public static final String FACTORY_DATA_ID = "lw_omnifac_allfactories";
    public static final String STATION_ID = "omnifac";
    public static final String STATION_NAME = "Remnant Autofactory";
    public static final String STATION_FACTION = "player";
    public static final String SUBMARKET_ID = "omnifac_market";
    public static final String SUBMARKET_NAME = "Autofactory";

    private Constants() {
    }
}
