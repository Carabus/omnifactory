package org.lazywizard.omnifac.secondary;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.combat.ShipHullSpecAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.util.Pair;
import org.lazywizard.omnifac.Shipyard;
import org.lazywizard.omnifac.ShipyardData;

import java.awt.Color;
import java.util.ArrayList;

import static com.fs.starfarer.api.combat.ShieldAPI.ShieldType.PHASE;


/**
 * Created by Isaac on 6/4/17.
 */
public class ShipUIPlugin extends OmniFacCustomUIPlugin {
    static {
        SIZE = 200;
    }

    private ArrayList<ShipHullSpecAPI> hulls;
    private boolean capitals = true, cruisers = true, destroyers = true, frigates = true, carriers = true, lineCombatants = true, phaseShips = true;
    private ArrayList<ShipHullSpecAPI> exception;

    public ShipUIPlugin(Shipyard omnifacPlugin, InteractionDialogAPI dialogAPI) {
        super(omnifacPlugin, dialogAPI);
        hulls = new ArrayList<>();
        exception = new ArrayList<>();
        for (ShipyardData data : getOmnifac().getShipData().values()) {
            String hullId = data.getId().endsWith("_Hull") ? data.getId().substring(0, data.getId().length() - 5) : data.getId();
            hulls.add(Global.getSettings().getHullSpec(hullId));
        }
        for (ShipHullSpecAPI ship : hulls) {
            ArrayList<SpriteAPI> list = new ArrayList<>();
            try {
                list.add(Global.getSettings().getSprite(ship.getSpriteName()));
            } catch (Exception e) {
                exception.add(ship);
                // TODO: Add log4j integration.
            } finally {
                getSprites().put(ship.getBaseHullId(), list);
            }
        }
        for (ShipHullSpecAPI e : exception)
            getText().addParagraph("Error with " + e.getBaseHullId());
    }

    @Override
    public void renderInternal(float alphaMult, float x, float y, float w, float h) {
        SpriteAPI sprite;
        float u = x, v = y + h - SIZE;
        int skip = (int) ((getDy() % SIZE - getDy()) / SIZE * w / SIZE);
        Boolean first = getDisplay().isEmpty();
        Color color = Color.red;

        for (ShipHullSpecAPI hull : hulls) {
            if (skip-- > 0)
                continue;

            boolean doIt = false, secondary = false;
            switch (hull.getHullSize()) {
                case FRIGATE:
                    doIt = frigates;
                    break;
                case DESTROYER:
                    doIt = destroyers;
                    break;
                case CRUISER:
                    doIt = cruisers;
                    break;
                case CAPITAL_SHIP:
                    doIt = capitals;
                    break;
            }
            secondary = (hull.getDefenseType() == PHASE && phaseShips)
                || (hull.getFighterBays() <= 0 ? lineCombatants : carriers);
            doIt = doIt && secondary;
            if (doIt && v >= y) {
                if (first)
                    getDisplay().put(new Pair<>(u - x, v - y), getOmnifac().getShipData().get(hull.getHullName()));

                if ((sprite = getSprites().get(hull.getBaseHullId()).get(0)) == null)
                    continue;
                sprite.setAlphaMult(alphaMult);
                int activeSize = SIZE - 5;
                if (sprite.getWidth() > activeSize) {
                    sprite.setHeight(sprite.getHeight() * activeSize / sprite.getWidth());
                    sprite.setWidth(activeSize);
                }
                if (sprite.getHeight() > activeSize) {
                    sprite.setWidth(sprite.getWidth() * activeSize / sprite.getHeight());
                    sprite.setHeight(activeSize);
                }
                sprite.renderAtCenter(u + SIZE / 2, v + SIZE / 2);

                GLSquare(color, u + 5, v - 5, activeSize, activeSize, alphaMult);

                u += SIZE;
                if (u + SIZE > x + getP().getWidth()) {
                    v -= SIZE;
                    u = x;
                }
            }
        }
    }

}