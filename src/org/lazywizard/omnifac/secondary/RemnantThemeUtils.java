package org.lazywizard.omnifac.secondary;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.impl.campaign.events.OfficerManagerEvent;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2;
import com.fs.starfarer.api.impl.campaign.ids.*;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.RemnantSeededFleetManager;
import com.fs.starfarer.api.util.WeightedRandomPicker;

import static com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator.random;
import static com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator.LocationType.PLANET_ORBIT;
import static com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator.setEntityLocation;
import static com.fs.starfarer.api.impl.campaign.procgen.themes.RemnantThemeGenerator.addRemnantStationInteractionConfig;


public final class RemnantThemeUtils {
    public static CampaignFleetAPI addBattlestation(StarSystemAPI system, PlanetAPI toOrbit, WeightedRandomPicker<String> stationTypes) {
        BaseThemeGenerator.EntityLocation loc = new BaseThemeGenerator.EntityLocation();
        loc.location = toOrbit.getLocation();
        loc.orbit = toOrbit.getOrbit();
        loc.type = PLANET_ORBIT;

        String type = stationTypes.pick();
        CampaignFleetAPI fleet = FleetFactoryV2.createEmptyFleet(Factions.REMNANTS, FleetTypes.BATTLESTATION, null);

        FleetMemberAPI member = Global.getFactory().createFleetMember(FleetMemberType.SHIP, type);
        fleet.getFleetData().addFleetMember(member);

        //fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_PIRATE, true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_MAKE_AGGRESSIVE, true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_NO_JUMP, true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_MAKE_ALLOW_DISENGAGE, true);

        fleet.setStationMode(true);

        addRemnantStationInteractionConfig(fleet);

        system.addEntity(fleet);

        //fleet.setTransponderOn(true);
        fleet.clearAbilities();
        fleet.addAbility(Abilities.TRANSPONDER);
        fleet.getAbility(Abilities.TRANSPONDER).activate();
        fleet.getDetectedRangeMod().modifyFlat("gen", 1000f);

        fleet.setAI(null);

        setEntityLocation(fleet, loc, null);
        convertOrbitWithSpin(fleet, 5f);

        boolean damaged = type.toLowerCase().contains("damaged");
        float mult = 25f;
        int level = 20;
        if (damaged) {
            mult = 10f;
            level = 10;
            fleet.getMemoryWithoutUpdate().set("$damagedStation", true);
        } //else {
        PersonAPI commander = OfficerManagerEvent.createOfficer(
            Global.getSector().getFaction(Factions.REMNANTS), level, true);
        if (!damaged) {
            commander.getStats().setSkillLevel(Skills.GUNNERY_IMPLANTS, 3);
        }
        FleetFactoryV2.addCommanderSkills(commander, fleet, random);
        fleet.setCommander(commander);
        fleet.getFlagship().setCaptain(commander);
        //}

        member.getRepairTracker().setCR(member.getRepairTracker().getMaxCR());

        RemnantSeededFleetManager.addRemnantAICoreDrops(random, fleet, mult);

        return fleet;
    }

    public static void convertOrbitWithSpin(SectorEntityToken entity, float spin) {
        SectorEntityToken focus = entity.getOrbitFocus();
        if (focus != null) {
            float angle = entity.getCircularOrbitAngle();
            float period = entity.getCircularOrbitPeriod();
            float radius = entity.getCircularOrbitRadius();
            entity.setCircularOrbitWithSpin(focus, angle, radius, period, spin, spin);
            ((CircularOrbitWithSpinAPI) entity.getOrbit()).setSpinVel(spin);
        }
    }
}
