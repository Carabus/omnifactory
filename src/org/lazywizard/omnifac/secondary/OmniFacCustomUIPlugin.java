package org.lazywizard.omnifac.secondary;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CustomUIPanelPlugin;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.TextPanelAPI;
import com.fs.starfarer.api.campaign.econ.CommoditySpecAPI;
import com.fs.starfarer.api.graphics.PositionAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.util.Pair;
import org.lazywizard.omnifac.OmniFacSettings;
import org.lazywizard.omnifac.Shipyard;
import org.lazywizard.omnifac.ShipyardData;
import org.lwjgl.opengl.GL11;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Isaac on 6/4/17.
 */
public abstract class OmniFacCustomUIPlugin implements CustomUIPanelPlugin {
    protected static int SIZE;
    private PositionAPI p;
    private Shipyard omnifac;
    private HashMap<String, ArrayList<SpriteAPI>> sprites;
    private HashMap<Pair<Float, Float>, ShipyardData> display;
    private TextPanelAPI text;
    private InteractionDialogAPI dialog;
    private int dx;
    private int dy;

    private float mouseX, mouseY;

    public OmniFacCustomUIPlugin(Shipyard omnifacPlugin, InteractionDialogAPI dialogAPI) {
        setDx(0);
        setDy(0);
        setDisplay(new HashMap<Pair<Float, Float>, ShipyardData>());
        setText(dialogAPI.getTextPanel());
        setDialog(dialogAPI);
        setSprites(new HashMap<String, ArrayList<SpriteAPI>>());
        setOmnifac(omnifacPlugin);
    }

    @Override
    public void positionChanged(PositionAPI position) {
        setP(position);
        mouseX = getP().getX() + getP().getWidth() / 2f;
        mouseY = getP().getY() + getP().getHeight() / 2f;
    }

    @Override
    public void advance(float amount) {
        if (getP() == null) return;
    }

    @Override
    public void processInput(List<InputEventAPI> events) {
        if (getP() == null) return;

        for (InputEventAPI event : events) {
            if (event.isConsumed()) continue;

            if (event.isLMBDownEvent()) {
                //text.addParagraph("x,y: " + event.getX() + "," + event.getY());
                if (!inSquare(event.getX(), event.getY(), getP().getX(), getP().getY(), getP().getWidth(), getP().getHeight()))
                    //dialog.getVisualPanel().fadeVisualOut();
                    return;
                float x = event.getX() - getP().getX();
                x -= x % SIZE;
                float y = event.getY() - getP().getY();
                y -= y % SIZE;
                ShipyardData data = getDisplay().get(new Pair<>(x, y));
                if (data != null && data.getCost() != null) {
                    getText().addParagraph("");
                    getText().addParagraph(data.getDisplayName() + " Resource Requirements:");
                    float totalCost = 0;
                    for (Map.Entry<String, Integer> commodity : data.getCost().entrySet()) {
                        if (commodity.getValue() * OmniFacSettings.getCostMult() > 0) {
                            float amount = commodity.getValue() * OmniFacSettings.getCostMult();
                            CommoditySpecAPI commoditySpec = Global.getSector().getEconomy().getCommoditySpec(commodity.getKey());
                            getText().addParagraph(Math.round(amount) + " " + commoditySpec.getName());
                            totalCost += amount * commoditySpec.getBasePrice();
                        }
                    }
                    getText().addParagraph("Approximate total cost of resources: " + Math.round(totalCost));
                    getText().highlightInLastPara(Color.YELLOW, String.valueOf(Math.round(totalCost)));
                    getText().addParagraph("Base " + data.getDisplayType() + " value: " + data.getBaseCost());
                    getText().highlightInLastPara(Color.YELLOW, String.valueOf(data.getBaseCost()));
                }
                event.consume();
            }

            if (event.isMouseScrollEvent()) {
                if (inSquare(event.getX(), event.getY(), getP().getX(), getP().getY(), getP().getWidth(), getP().getHeight())) {
                    setDy(getDy() + (event.getEventValue() > 0 ? 200 : -200));
                    getDisplay().clear();
                    event.consume();
                }
            }

            if (event.isMouseMoveEvent()) {
                if (getP().containsEvent(event)) {
                    mouseX = event.getX();
                    mouseY = event.getY();
                } else {
                    mouseX = getP().getX() + getP().getWidth() / 2f;
                    mouseY = getP().getY() + getP().getHeight() / 2f;
                }
            }
        }
    }

    @Override
    public void render(float alphaMult) {
        if (getP() == null) return;

        float x = getP().getX();
        float y = getP().getY();
        float w = getP().getWidth();
        float h = getP().getHeight();

        Color color = Color.cyan;

        GLSquare(color, x, y, w + 5, h + 5, alphaMult);

        renderInternal(alphaMult, x, y, w, h);
    }

    public abstract void renderInternal(float alphaMult, float x, float y, float w, float h);

    private boolean inSquare(float u, float v, float x, float y, float w, float h) {
        return u > x && u < x + w && v > y && v < y + h;
    }

    public void GLSquare(Color color, float x, float y, float w, float h, float alphaMult) {
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        GL11.glColor4ub((byte) color.getRed(),
            (byte) color.getGreen(),
            (byte) color.getBlue(),
            (byte) (color.getAlpha() * alphaMult * 0.25f));

        GL11.glBegin(GL11.GL_QUADS);
        {
            GL11.glVertex2f(x, y);
            GL11.glVertex2f(x, y + h);
            GL11.glVertex2f(x + w, y + h);
            GL11.glVertex2f(x + w, y);
        }
        GL11.glEnd();
    }

    public PositionAPI getP() {
        return p;
    }

    public void setP(PositionAPI p) {
        this.p = p;
    }

    public Shipyard getOmnifac() {
        return omnifac;
    }

    public void setOmnifac(Shipyard omnifac) {
        this.omnifac = omnifac;
    }

    public HashMap<String, ArrayList<SpriteAPI>> getSprites() {
        return sprites;
    }

    public void setSprites(HashMap<String, ArrayList<SpriteAPI>> sprites) {
        this.sprites = sprites;
    }

    public HashMap<Pair<Float, Float>, ShipyardData> getDisplay() {
        return display;
    }

    public void setDisplay(HashMap<Pair<Float, Float>, ShipyardData> display) {
        this.display = display;
    }

    public TextPanelAPI getText() {
        return text;
    }

    public void setText(TextPanelAPI text) {
        this.text = text;
    }

    public int getDx() {
        return dx;
    }

    public void setDx(int dx) {
        this.dx = dx;
    }

    public int getDy() {
        return dy;
    }

    public void setDy(int dy) {
        this.dy = dy;
    }

    public InteractionDialogAPI getDialog() {
        return dialog;
    }

    public void setDialog(InteractionDialogAPI dialog) {
        this.dialog = dialog;
    }
}