package org.lazywizard.omnifac.secondary;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.loading.WeaponSpecAPI;
import com.fs.starfarer.api.util.Pair;
import org.json.JSONObject;
import org.lazywizard.omnifac.Shipyard;
import org.lazywizard.omnifac.ShipyardData;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;


/**
 * Created by Isaac on 6/4/17.
 */
public class WeaponUIPlugin extends OmniFacCustomUIPlugin {
    static {
        SIZE = 100;
    }

    private ArrayList<WeaponSpecAPI> weps;
    private boolean small = true, medium = true, large = true, ballistic = true, energy = true, missile = true;
    private ArrayList<WeaponSpecAPI> exception;

    public WeaponUIPlugin(Shipyard omnifacPlugin, InteractionDialogAPI dialogAPI) {
        super(omnifacPlugin, dialogAPI);
        exception = new ArrayList<>();
        weps = new ArrayList<>();
        for (ShipyardData data : getOmnifac().getWepData().values())
            weps.add(Global.getSettings().getWeaponSpec(data.getId()));
        for (WeaponSpecAPI wep : weps) {
            ArrayList<SpriteAPI> sprites = new ArrayList<>();
            try {
                JSONObject json = Global.getSettings().loadJSON("data/weapons/" + wep.getWeaponId() + ".wpn");
                Iterator<String> iter = json.keys();
                while (iter.hasNext()) {
                    String tmp = iter.next();
                    if (tmp.contains("turret") && tmp.contains("Sprite")) {
                        sprites.add(Global.getSettings().getSprite(json.getString(tmp)));
                    }
                }
            } catch (Exception e) {
                exception.add(wep);
                // TODO: Use log4j to report error.
            } finally {
                getSprites().put(wep.getWeaponId(), sprites);
            }
        }
        for (WeaponSpecAPI e : exception)
            getText().addParagraph("Error with " + e.getWeaponId());
    }

    @Override
    public void renderInternal(float alphaMult, float x, float y, float w, float h) {
        float u = x, v = y + h - SIZE;
        // Skip an integer number of weapons if the display area doesn't divide evenly.
        int skip = (int) ((getDy() % SIZE - getDy()) / SIZE * w / SIZE);
        Boolean first = getDisplay().isEmpty();
        Color color = Color.red;

        for (WeaponSpecAPI wep : weps) {
            if (skip-- > 0)
                continue;

            boolean doIt = false, secondary = false;
            switch (wep.getSize()) {
                case SMALL:
                    doIt = small;
                    break;
                case MEDIUM:
                    doIt = medium;
                    break;
                case LARGE:
                    doIt = large;
                    break;
            }
            switch (wep.getType()) {
                case ENERGY:
                    secondary = energy;
                    break;
                case MISSILE:
                    secondary = missile;
                    break;
                case BALLISTIC:
                    secondary = ballistic;
                    break;
            }
            doIt = doIt && secondary;

            if (doIt && v >= y) {
                int activeSize = SIZE - 5;
                if (first)
                    getDisplay().put(new Pair<>(u - x, v - y), getOmnifac().getWepData().get(wep.getWeaponId()));

                for (SpriteAPI sprite : getSprites().get(wep.getWeaponId())) {
                    if (sprite == null)
                        continue;
                    sprite.setAlphaMult(alphaMult);
                    if (sprite.getWidth() > activeSize) {
                        sprite.setHeight(sprite.getHeight() * activeSize / sprite.getWidth());
                        sprite.setWidth(activeSize);
                    }
                    if (sprite.getHeight() > activeSize) {
                        sprite.setWidth(sprite.getWidth() * activeSize / sprite.getHeight());
                        sprite.setHeight(activeSize);
                    }
                    sprite.renderAtCenter(u + SIZE / 2, v + SIZE / 2);
                }

                GLSquare(color, u + 5, v + 5, activeSize, activeSize, alphaMult);
                u += SIZE;
                if (u + SIZE > x + getP().getWidth()) {
                    v -= SIZE;
                    u = x;
                }
            }
        }
    }
}