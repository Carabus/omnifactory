package org.lazywizard.omnifac.secondary;

/**
 * Created by Isaac on 6/4/17.
 */

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.campaign.events.CampaignEventPlugin;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.campaign.rules.RulesAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipHullSpecAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.DevMenuOptions;
import com.fs.starfarer.api.impl.campaign.PlanetInteractionDialogPluginImpl;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.impl.campaign.rulecmd.DismissDialog;
import com.fs.starfarer.api.impl.campaign.rulecmd.DumpMemory;
import com.fs.starfarer.api.impl.campaign.rulecmd.FireAll;
import com.fs.starfarer.api.impl.campaign.rulecmd.FireBest;
import com.fs.starfarer.api.impl.campaign.submarkets.MilitarySubmarketPlugin;

import java.awt.Color;
import java.text.NumberFormat;
import java.util.*;

import static com.fs.starfarer.api.combat.ShipHullSpecAPI.ShipTypeHints.CIVILIAN;
import static com.fs.starfarer.api.combat.ShipHullSpecAPI.ShipTypeHints.HIDE_IN_CODEX;
import static com.fs.starfarer.api.combat.ShipHullSpecAPI.ShipTypeHints.UNBOARDABLE;
import static org.lazywizard.omnifac.OmniFacSettings.getRestrictedMarkets;


public class MarketSearchInteractionDialog extends PlanetInteractionDialogPluginImpl implements RuleBasedDialog {
    public static final String FAILSAFE_LEAVE = "rbid_failsafe_leave";


    private static enum OptionId {
        INIT,
        LEAVE,
        SEARCH,
        CAPITAL,
        CRUISER,
        DESTROYER,
        FRIGATE,
        CARRIER,
        NOT_CARRIER,
        CIVILIAN,
        NOT_CIVILIAN,
    }


    private InteractionDialogAPI dialog;
    private TextPanelAPI textPanel;
    private OptionPanelAPI options;
    private VisualPanelAPI visual;

    private RulesAPI rules;
    private MemoryAPI memory;
    private Map<String, MemoryAPI> memoryMap;

    private boolean embeddedMode = false;

    public void setEmbeddedMode(boolean embeddedMode) {
        this.embeddedMode = embeddedMode;
    }

    private final String initialTrigger;

    private CampaignFleetAPI playerFleet;
    FleetMemberAPI member;
    private PlanetAPI planet;
    private OrderData found;
    private ShipAPI.HullSize hullSize = ShipAPI.HullSize.DEFAULT;
    private int carrier = -1;
    private int civilian = -1;

    private static final Color HIGHLIGHT_COLOR = Global.getSettings().getColor("buttonShortcut");

    public MarketSearchInteractionDialog() {
        this("OpenInteractionDialog");
    }

    public MarketSearchInteractionDialog(String initialTrigger) {
        this.initialTrigger = initialTrigger;
    }

    @Override
    public void reinit(boolean withContinueOnRuleFound) {
        init(dialog);
    }


    public boolean fireAll(String trigger) {
        return FireAll.fire(null, dialog, memoryMap, trigger);
    }

    public boolean fireBest(String trigger) {
        return FireBest.fire(null, dialog, memoryMap, trigger);
    }

    @Override
    public void init(InteractionDialogAPI dialog) {
        this.dialog = dialog;

        textPanel = dialog.getTextPanel();
        options = dialog.getOptionPanel();
        visual = dialog.getVisualPanel();

        playerFleet = Global.getSector().getPlayerFleet();
        planet = (PlanetAPI) dialog.getInteractionTarget();

        visual.setVisualFade(0.25f, 0.25f);

        if (!embeddedMode) {
            visual.setVisualFade(0.25f, 0.25f);
        }

        rules = Global.getSector().getRules();

        updateMemory();

        optionSelected(null, OptionId.INIT);
    }

    @Override
    public void optionSelected(String text, Object optionData) {
        if (optionData == null) return;

        String optionId = optionData instanceof OptionId ? null : (String) optionData;
        OptionId option = optionData instanceof OptionId ? (OptionId) optionData : null;

        if (optionId != null && optionId.equals("search")) {
            found = new OrderData(null, null, Float.MAX_VALUE, 0, false);
            options.clearOptions();
            textPanel.addParagraph("The local UCS Agency offers market search servies for wealthy clients. Would you like to make an order?");
            if (playerFleet.getCargo().getCredits().get() >= 10000f) {
                options.addOption("Yes", OptionId.SEARCH, "10000 Credits");
                options.addOption("No", OptionId.INIT, null);
            } else
                options.addOption("You don't have enought money!", OptionId.INIT, "10000 Credits");
            return;
        }

        if (found != null && found.getShip() != null && optionId != null && optionId.equals("buyS")) {
            if (playerFleet.getCargo().getCredits().get() >= found.getPrice()) {
                playerFleet.getCargo().getCredits().subtract(found.getPrice());
                found.getMarket().getCargo().getMothballedShips().removeFleetMember(found.getShip());
                SubmarketAPI storage = null;
                for (SubmarketAPI submarket : planet.getMarket().getSubmarketsCopy())
                    if (submarket.getPlugin().isFreeTransfer())
                        storage = submarket;
                if (storage == null)
                    playerFleet.getCargo().getMothballedShips().addFleetMember(found.getShip());
                else
                    storage.getCargo().getMothballedShips().addFleetMember(found.getShip());
                textPanel.addParagraph("Thank you for your business. The " + found.getShip().getHullSpec().getHullName() + " will de deposited into local storage in X days.");
            } else
                textPanel.addParagraph("You cannot afford that!");
            option = OptionId.INIT;
        }

        if (optionId == FAILSAFE_LEAVE) {
            new DismissDialog().execute(null, dialog, null, memoryMap);
            return;
        }

        if (optionId == DumpMemory.OPTION_ID) {
            new DumpMemory().execute(null, dialog, null, memoryMap);
            return;
        } else if (DevMenuOptions.isDevOption(optionData)) {
            DevMenuOptions.execute(dialog, (String) optionData);
            return;
        }

        if (text != null)
            textPanel.addParagraph(text, Global.getSettings().getColor("buttonText"));

        if (hullSize != ShipAPI.HullSize.DEFAULT && carrier >= 0 && civilian >= 0) {
            purchaseSearch(optionId);
            option = OptionId.INIT;
            hullSize = ShipAPI.HullSize.DEFAULT;
            carrier = -1;
            civilian = -1;
            if (options.hasOption("buyS"))
                return;
        }

        if (option != null && option.equals(OptionId.SEARCH)) {
            playerFleet.getCargo().getCredits().subtract(10000f);
            options.clearOptions();
            textPanel.addParagraph("You pay the 10,000 credit fee. Which type of ship do you search for?");
            options.addOption("Capital", OptionId.CAPITAL, null);
            options.addOption("Cruiser", OptionId.CRUISER, null);
            options.addOption("Destroyer", OptionId.DESTROYER, null);
            options.addOption("Frigate", OptionId.FRIGATE, null);
            return;
        }

        if (option != null) switch (option) {
            case INIT:
                createInitialOptions();
                break;
            case LEAVE:
                Global.getSector().setPaused(false);
                dialog.dismiss();
                break;
            case CIVILIAN:
                if (civilian < 0)
                    civilian = 1;
            case NOT_CIVILIAN:
                if (civilian < 0)
                    civilian = 0;
            case CARRIER:
                if (carrier < 0)
                    carrier = 1;
            case NOT_CARRIER:
                if (carrier < 0)
                    carrier = 0;
            case CAPITAL:
                if (hullSize == ShipAPI.HullSize.DEFAULT)
                    hullSize = ShipAPI.HullSize.CAPITAL_SHIP;
            case CRUISER:
                if (hullSize == ShipAPI.HullSize.DEFAULT)
                    hullSize = ShipAPI.HullSize.CRUISER;
            case DESTROYER:
                if (hullSize == ShipAPI.HullSize.DEFAULT)
                    hullSize = ShipAPI.HullSize.DESTROYER;
            case FRIGATE:
                if (hullSize == ShipAPI.HullSize.DEFAULT)
                    hullSize = ShipAPI.HullSize.FRIGATE;
            default:
                if (hullSize != ShipAPI.HullSize.DEFAULT && carrier < 0) {
                    options.clearOptions();
                    textPanel.addParagraph("Do you wish to search for a carrier or a ship of the line?");
                    options.addOption("Carriers", OptionId.CARRIER, null);
                    options.addOption("Ships of the Line", OptionId.NOT_CARRIER, null);
                    break;
                }
                if (hullSize != ShipAPI.HullSize.DEFAULT && carrier >= 0 && civilian < 0) {
                    options.clearOptions();
                    textPanel.addParagraph("Do you wish to search for a civilian or military vessel?");
                    options.addOption("Civilian", OptionId.CIVILIAN, null);
                    options.addOption("Military", OptionId.NOT_CIVILIAN, null);
                    break;
                }
                if (hullSize != ShipAPI.HullSize.DEFAULT && carrier >= 0 && civilian >= 0) {
                    options.clearOptions();
                    textPanel.addParagraph("Which ship do you wish to search for?");

                    ShipHullSpecAPI hull;
                    for (String id : Global.getSector().getAllEmptyVariantIds())
                        if ((hull = Global.getSettings().getHullSpec(id = id.substring(0, id.length() - 5))).getHullSize() == hullSize
                            && !(hull.getFighterBays() > 0 ^ carrier > 0) && !hull.isDHull() && !hull.getHints().contains(HIDE_IN_CODEX) && !hull.getHints().contains(UNBOARDABLE)
                            && !(hull.getHints().contains(CIVILIAN) ^ civilian > 0))
                            options.addOption(hull.getHullName(), id, null);
                }
                options.addOption("Leave", FAILSAFE_LEAVE);
                return;
        }

        memory.set("$option", optionId);
        memory.expire("$option", 0);

        fireBest("DialogOptionSelected");
    }

    private void createInitialOptions() {
        options.clearOptions();
        options.addOption("Leave", OptionId.LEAVE, null);

        if (Global.getSettings().isDevMode()) {
            DevMenuOptions.addOptions(dialog);
        }
        if (!embeddedMode) {
            fireBest(initialTrigger);
            if (!options.hasOptions()) {
                options.clearOptions();
                options.addOption("Leave", OptionId.LEAVE);
                if (Global.getSettings().isDevMode()) {
                    DevMenuOptions.addOptions(dialog);
                }
            }
        }
    }

    public void updateMemory() {
        if (memoryMap == null) {
            memoryMap = new HashMap<String, MemoryAPI>();
        } else {
            memoryMap.clear();
        }
        memory = dialog.getInteractionTarget().getMemory();

        memoryMap.put(MemKeys.LOCAL, memory);
        if (dialog.getInteractionTarget().getFaction() != null) {
            memoryMap.put(MemKeys.FACTION, dialog.getInteractionTarget().getFaction().getMemory());
        } else {
            memoryMap.put(MemKeys.FACTION, Global.getFactory().createMemory());
        }
        memoryMap.put(MemKeys.GLOBAL, Global.getSector().getMemory());
        memoryMap.put(MemKeys.PLAYER, Global.getSector().getCharacterData().getMemory());

        if (dialog.getInteractionTarget().getMarket() != null) {
            memoryMap.put(MemKeys.MARKET, dialog.getInteractionTarget().getMarket().getMemory());
        }

        if (memory.contains(MemFlags.MEMORY_KEY_SOURCE_MARKET)) {
            String marketId = memory.getString(MemFlags.MEMORY_KEY_SOURCE_MARKET);
            MarketAPI market = Global.getSector().getEconomy().getMarket(marketId);
            if (market != null) {
                memoryMap.put(MemKeys.SOURCE_MARKET, market.getMemory());
            }
        }

        updatePersonMemory();
    }

    private void updatePersonMemory() {
        PersonAPI person = dialog.getInteractionTarget().getActivePerson();
        if (person != null) {
            memory = person.getMemory();
            memoryMap.put(MemKeys.LOCAL, memory);
            memoryMap.put(MemKeys.PERSON_FACTION, person.getFaction().getMemory());
            memoryMap.put(MemKeys.ENTITY, dialog.getInteractionTarget().getMemory());
        } else {
            memory = dialog.getInteractionTarget().getMemory();
            memoryMap.put(MemKeys.LOCAL, memory);
            memoryMap.remove(MemKeys.ENTITY);
            memoryMap.remove(MemKeys.PERSON_FACTION);

        }
    }

    public void notifyActivePersonChanged() {
        updatePersonMemory();
    }

    public void setActiveMission(CampaignEventPlugin mission) {
        if (mission == null) {
            memoryMap.remove(MemKeys.MISSION);
        } else {
            MemoryAPI memory = mission.getMemory();
            if (memory != null) {
                memoryMap.put(MemKeys.MISSION, memory);
            } else {
                memoryMap.remove(MemKeys.MISSION);
            }
        }
    }

    private RepLevel getRequiredLevelAssumingLegal(FleetMemberAPI member, SubmarketPlugin.TransferAction action) {
        if (action == SubmarketPlugin.TransferAction.PLAYER_BUY) {
            int fp = member.getFleetPointCost();
            ShipAPI.HullSize size = member.getHullSpec().getHullSize();

            if (size == ShipAPI.HullSize.CAPITAL_SHIP || fp > 15) return RepLevel.COOPERATIVE;
            if (size == ShipAPI.HullSize.CRUISER || fp > 10) return RepLevel.FRIENDLY;
            if (size == ShipAPI.HullSize.DESTROYER || fp > 5) return RepLevel.WELCOMING;
            return RepLevel.FAVORABLE;
        }
        return null;
    }

    private void purchaseSearch(String hullId) {
        for (String id : getRestrictedMarkets())
            textPanel.addParagraph(id);
        final List<OrderData> finds = new ArrayList<>();
        lab:
        for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy()) {
            if (!getRestrictedMarkets().contains(market.getId()) && !getRestrictedMarkets().contains(market.getPrimaryEntity().getId()))
                for (SubmarketAPI submarket : market.getSubmarketsCopy()) {
//                  textPanel.addParagraph("Searching " + market.getId() + ":" + submarket.getSpecId() + " for " + hullId);
                    if (submarket.getPlugin() == null || getRestrictedMarkets().contains(submarket.getSpecId()))
                        continue;
                    submarket.getPlugin().updateCargoPrePlayerInteraction();
                    int total = 0;
                    float price = 0f;
                    boolean isIllegal = false;
                    for (FleetMemberAPI member : submarket.getCargo().getMothballedShips().getSnapshot()) {
                        if (member.getHullId().equalsIgnoreCase(hullId)) {
                            this.member = member;
                            if (submarket.getPlugin().isFreeTransfer())
                                continue;
                            total++;
                            price = member.getBaseBuyValue();
                            price += (price * submarket.getTariff());
                            price *= 1.25;
                            if (submarket.getPlugin() instanceof MilitarySubmarketPlugin)
                                switch (getRequiredLevelAssumingLegal(member, SubmarketPlugin.TransferAction.PLAYER_BUY)) {
                                    case FAVORABLE:
                                        price *= 1.25;
                                        break;
                                    case WELCOMING:
                                        price *= 1.5;
                                        break;
                                    case FRIENDLY:
                                        price *= 1.75;
                                        break;
                                    case COOPERATIVE:
                                        price *= 2;
                                        break;
                                }
                            if (member.getHullId().contains("tem_"))
                                price *= 3;
                            isIllegal = submarket.getPlugin().isIllegalOnSubmarket(member, SubmarketPlugin.TransferAction.PLAYER_BUY);
                        }
                    }
                    if (total > 0)
                        finds.add(new OrderData(submarket, member, price, total, isIllegal));
                }
        }
        textPanel.addParagraph("Size = " + finds.size());
        for (OrderData find : finds)
            found = find.getPrice() < found.getPrice() ? find : found;
        if (!finds.isEmpty()) {
            textPanel.addParagraph("A " + found.getShip().getHullSpec().getHullName() + " has been found on " + found.getMarket().getMarket().getPrimaryEntity().getName() + " for " + found.getFormattedPrice() + " credits.");
            textPanel.appendToLastParagraph("Would you like to purchase it?");
            options.clearOptions();
            options.addOption("Yes", "buyS", null);
            options.addOption("No", OptionId.INIT, null);
            return;
        } else
            textPanel.addParagraph("The agency could not find the requested ship.");
    }

    public static class OrderData {
        private final SubmarketAPI market;
        private final FleetMemberAPI ship;
        private final float pricePer;
        private final int totalAvailable;
        private final boolean isIllegal;

        public OrderData(SubmarketAPI market, FleetMemberAPI ship, float pricePer, int totalAvailable, boolean isIllegal) {
            this.pricePer = pricePer;
            this.totalAvailable = totalAvailable;
            this.isIllegal = isIllegal;
            this.ship = ship;
            this.market = market;
        }

        public SubmarketAPI getMarket() {
            return market;
        }

        public FleetMemberAPI getShip() {
            return ship;
        }

        public float getPrice() {
            return pricePer;
        }

        public String getFormattedPrice() {
            return NumberFormat.getIntegerInstance().format(Math.round(pricePer));
        }

        public int getAvailable() {
            return totalAvailable;
        }

        public boolean isIllegal() {
            return isIllegal;
        }
    }

    private void addText(String text) {
        textPanel.addParagraph(text);
    }

    private void appendText(String text) {
        textPanel.appendToLastParagraph(" " + text);
    }

    private String getString(String id) {
        String str = Global.getSettings().getString("planetInteractionDialog", id);

        String fleetOrShip = "fleet";
        if (playerFleet.getFleetData().getMembersListCopy().size() == 1) {
            fleetOrShip = "ship";
            if (playerFleet.getFleetData().getMembersListCopy().get(0).isFighterWing()) {
                fleetOrShip = "fighter wing";
            }
        }
        str = str.replaceAll("\\$fleetOrShip", fleetOrShip);
        str = str.replaceAll("\\$planetName", planet.getName());

        return str;
    }
}



