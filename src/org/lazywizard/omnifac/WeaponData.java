package org.lazywizard.omnifac;

import com.fs.starfarer.api.campaign.CargoStackAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.loading.WeaponSpecAPI;

import java.util.HashMap;


class WeaponData extends ShipyardData {
    int tier, opCost;

    WeaponData(Shipyard shipyard, CargoStackAPI stack) {
        super(shipyard);
        ordered = 0;
        cost = new HashMap<>();
        WeaponSpecAPI spec = stack.getWeaponSpecIfWeapon();
        id = (String) stack.getData();
        displayName = stack.getDisplayName();
        tier = spec.getTier() + 1;
        opCost = (int) spec.getOrdnancePointCost(null);
        size = stack.getCargoSpacePerUnit();
        //2 40
        //4 20
        //8 10
        stackSize = (int) (80 / size);
        lastUpdate = shipyard.getNumHeartbeats();
        baseCost = (int) spec.getBaseValue();

        int baseMaterials = (int) size / 2;
        int tierMaterials = baseMaterials * spec.getTier();

        cost.put(Commodities.METALS, getWeaponMaterialsAmount(spec));
        // base * 150 value + tier * 200 value
        if (spec.getType() == WeaponAPI.WeaponType.BALLISTIC) {
            cost.put(Commodities.HEAVY_MACHINERY, baseMaterials);
            cost.put(Commodities.RARE_METALS, tierMaterials);
        } else if (spec.getType() == WeaponAPI.WeaponType.MISSILE) {
            cost.put(Commodities.FUEL, 6 * baseMaterials +
                                       8 * tierMaterials);
        } else if (spec.getType() == WeaponAPI.WeaponType.ENERGY) {
            cost.put(Commodities.VOLATILES, 5 * baseMaterials);
            cost.put(Commodities.RARE_METALS, tierMaterials);
        }
    }

    private int getWeaponMaterialsAmount(WeaponSpecAPI spec) {
        int baseMaterials = 1 + (int) spec.getOrdnancePointCost(null);
        switch (spec.getSize()) {
            default:
            case SMALL:
                return baseMaterials;
            case MEDIUM:
                return baseMaterials + Math.round(baseMaterials * 0.5f);
            case LARGE:
                return baseMaterials + Math.round(baseMaterials * 0.5f) * 2;
        }
    }

    @Override
    public String getDisplayType() {
        return "weapon";
    }

    @Override
    public int getDaysToAnalyze() {
        return (int) Math.max(1f,
                getBaseDays() * OmniFacSettings.getWeaponAnalysisTimeMod());
    }

    @Override
    protected int getBaseDays() {
        return Math.round((1 + opCost) * (1 + tier) * 0.4f);
    }

    @Override
    public int getDaysToCreate() {
        return (int) Math.max(getBaseDays() * OmniFacSettings.getWeaponProductionTimeMod(), 1f);
    }

//        @Override
//        public int getTotal()
//        {
//            return getStorageCargo().getNumWeapons(id);
//        }

    @Override
    public int getLimit() {
        return (int) (stackSize * OmniFacSettings.getMaxStacksPerWeapon());
    }

    @Override
    public boolean create(boolean really) throws NullPointerException {
        lastUpdate = shipyard.getNumHeartbeats();

        if (getTotal() >= getLimit()) {
            return false;
        }

        warnedLimit = false;
        if (really) {
            shipyard.getStorageCargo().addWeapons(id, 1);
            ordered--;
        } else {
            if (OmniFacSettings.shouldReturnAnalyzedWeapon()) {
                shipyard.getStorageCargo().addWeapons(id, 1);
            }
            shipyard.getFactoryCargo().addWeapons(id, getLimit() - shipyard.getFactoryCargo().getNumWeapons(id));
        }
        return true;
    }
}
