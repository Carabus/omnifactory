package org.lazywizard.omnifac;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.submarkets.StoragePlugin;
import org.apache.log4j.Logger;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.campaign.MessageUtils;
import org.lazywizard.omnifac.secondary.Constants;

import java.util.*;


public abstract class Shipyard extends StoragePlugin {
    // Strings
    protected static final String SHIP_NAME = "ship", WEAPON_NAME = "weapon", WING_NAME = "LPC";
    protected static final String NO_REPLICATE = "Unable to Replicate.";
    protected static final String HAS_BLUEPRINT = "Blueprint already known";
    protected static final String GAMMA_CORE_NEEDED = "Requires Gamma Class AI Core.";
    protected static final String GAMMA_CORES_NEEDED = "Requires 2 Gamma Class AI Cores.";
    protected static final String BETA_CORE_NEEDED = "Requires Beta Class AI Core.";
    protected static final String ALPHA_CORE_NEEDED = "Requires Alpha Class AI Core.";
    protected static final String ALPHA_CORES_NEEDED = "Requires 5 Alpha Class AI Cores.";
    // Privates
    private static Logger log = Global.getLogger(Shipyard.class);
    private final Map<String, ShipyardData> shipData = new HashMap<>();
    private final Map<String, ShipyardData> wepData = new HashMap<>();
    private final Map<String, ShipyardData> wingData = new HashMap<>();
    private SectorEntityToken station;
    private boolean displayCosts = false;
    private boolean warnedRequirements = true, refit = false;
    private long lastHeartbeat;
    private int numHeartbeats = 0;

    @Override
    public void init(SubmarketAPI submarket) {
        super.init(submarket);
        super.setPlayerPaidToUnlock(true);
//      CampaignEngine.getInstance().getCampaignUI().getCore().setListener(new AutofactoryInteractionDialog());

        // Will be properly set by initOmnifactory(), but here as a fallback
        this.station = submarket.getMarket().getPrimaryEntity();

        // Synchronize factory heartbeat to the start of the next day
        final CampaignClockAPI clock = Global.getSector().getClock();
        lastHeartbeat = new GregorianCalendar(clock.getCycle(),
            clock.getMonth() - 1, clock.getDay()).getTimeInMillis();
    }

    public List<String> getKnownShips() {
        return new ArrayList<>(getShipData().keySet());
    }

    public List<String> getKnownWings() {
        return new ArrayList<>(getWingData().keySet());
    }

    public List<String> getKnownWeapons() {
        return new ArrayList<>(getWepData().keySet());
    }

    public ShipyardData getShipBlueprint(String hullOrWingId) {
        return getShipData().get(hullOrWingId);
    }

    public ShipyardData getWeaponBlueprint(String weaponId) {
        return getWepData().get(weaponId);
    }

    public SubmarketAPI getStorageMarket() {
        if (!market.hasSubmarket(Submarkets.SUBMARKET_STORAGE)) {
            market.addSubmarket(Submarkets.SUBMARKET_STORAGE);
            ((StoragePlugin) market.getSubmarket(Submarkets.SUBMARKET_STORAGE)
                .getPlugin()).setPlayerPaidToUnlock(true);
            market.addSubmarket(Constants.SUBMARKET_ID);
        }

        return market.getSubmarket(Submarkets.SUBMARKET_STORAGE);
    }

    public CargoAPI getFactoryCargo() {
        return getCargo();
    }

    public CargoAPI getStorageCargo() {
        return getStorageMarket().getCargo();
    }

    public SectorEntityToken getStation() {
        return station;
    }

    protected void setStation(SectorEntityToken station) {
        this.station = station;
    }

    public String getLocationString() {
        OrbitAPI orbit = station.getOrbit();
        LocationAPI loc = station.getContainingLocation();
        if (orbit == null || orbit.getFocus() == null) {
            return "orbiting nothing in " + (loc == null ? " nowhere" : loc.getName());
        }

        return "orbiting " + orbit.getFocus().getName() + " in " + loc.getName();
    }

    public int getNumHeartbeats() {
        return numHeartbeats;
    }

    @Override
    public String toString() {
        return station.getName() + " " + getLocationString()
            + " (" + getShipData().size() + " ships, " + getWepData().size()
            + " weapons known)";
    }

    protected boolean checkTimerAnalyze(ShipyardData tmp) {
        return numHeartbeats - tmp.getDaysToAnalyze() >= tmp.getLastUpdate();
    }

    protected boolean checkTimerCreate(ShipyardData tmp) {
        return numHeartbeats - tmp.getDaysToCreate() >= tmp.getLastUpdate();
    }

    protected abstract void processBlueprint(ShipyardData tmp, List<String> analyzed, List<String> added, List<String> limit, String name);

    protected abstract void addBluePrint(List<String> newItems, String name);

    protected abstract boolean blockBluePrint(List<String> blocked, String name);

    protected abstract void orderedMessage(List<String> ordered, String name);

    protected abstract void producedMessage(List<String> added, String name);

    protected abstract void finishMessage(List<String> analyzed, String name);

    private void heartbeat() {
        numHeartbeats++;

        List<String> addedShips = new ArrayList<>(), addedWeps = new ArrayList<>(), addedWings = new ArrayList<>(),
            analyzedShips = new ArrayList<>(), analyzedWeps = new ArrayList<>(), analyzedWings = new ArrayList<>(),
            hitLimit = new ArrayList<>();

        for (ShipyardData tmp : getShipData().values())
            processBlueprint(tmp, analyzedShips, addedShips, hitLimit, SHIP_NAME);
        for (ShipyardData tmp : getWepData().values())
            processBlueprint(tmp, analyzedWeps, addedWeps, hitLimit, WEAPON_NAME);
        for (ShipyardData tmp : getWingData().values())
            processBlueprint(tmp, analyzedWings, addedWings, hitLimit, WING_NAME);

        if (OmniFacSettings.shouldShowAddedCargo()) {
            if (!addedShips.isEmpty())
                producedMessage(addedShips, SHIP_NAME);
            if (!addedWeps.isEmpty())
                producedMessage(addedWeps, WEAPON_NAME);
            if (!addedWings.isEmpty())
                producedMessage(addedWings, WING_NAME);
        }

        if (OmniFacSettings.shouldShowLimitReached() && !hitLimit.isEmpty()) {
            Collections.sort(hitLimit);
            MessageUtils.showMessage("The " + station.getName()
                    + " has reached its limit for the following goods:",
                CollectionUtils.implode(hitLimit) + ".", true);
        }

        if (OmniFacSettings.shouldShowAnalysisComplete()) {
            if (!analyzedShips.isEmpty())
                finishMessage(analyzedShips, SHIP_NAME);
            if (!analyzedWeps.isEmpty())
                finishMessage(analyzedWeps, WEAPON_NAME);
            if (!analyzedWings.isEmpty())
                finishMessage(analyzedWings, WING_NAME);
        }
    }

    protected void Warning(ShipyardData tmp, List<String> addedItems, List<String> hitLimit) throws NullPointerException {
        if (utilCreate(tmp, true)) {
            if (OmniFacSettings.shouldShowAddedCargo())
                addedItems.add(tmp.getDisplayName() + " (" + tmp.getTotal()
                    + "/" + (tmp.getTotal() + tmp.getProduction()) + ")");
        } else if (OmniFacSettings.shouldShowLimitReached() && !tmp.hasWarnedLimit()) {
            hitLimit.add(tmp.getDisplayName());
            tmp.setWarnedLimit(true);
        }
    }

    protected boolean utilCreate(ShipyardData tmp, boolean isReal) {
        try {
            return tmp.create(isReal);
        } catch (NullPointerException e) {
            // TODO: Integrate log4j
            return false;
        }
    }

    @Override
    public void advance(float amount) {
        CampaignClockAPI clock = Global.getSector().getClock();
        refit = false;

        if (clock.getElapsedDaysSince(lastHeartbeat) >= 1f) {
            lastHeartbeat = clock.getTimestamp();
            heartbeat();
        }
    }

    @Override
    public OnClickAction getOnClickAction(CoreUIAPI ui) {
        return OnClickAction.OPEN_SUBMARKET;
    }

    @Override
    public String getName() {
        return Constants.SUBMARKET_NAME;
    }

    @Override
    public String getBuyVerb() {
        return "Order";
    }

    @Override
    public String getSellVerb() {
        return "Analyse";
    }

    @Override
    public boolean isIllegalOnSubmarket(String commodityId, TransferAction action) {
        return true;
    }

    @Override
    public boolean isIllegalOnSubmarket(CargoStackAPI stack, TransferAction action) {
        return action == TransferAction.PLAYER_SELL;
    }

    @Override
    public boolean isIllegalOnSubmarket(FleetMemberAPI ship, TransferAction action) {
        return action == TransferAction.PLAYER_SELL;
    }

    @Override
    public float getTariff() {
        return OmniFacSettings.getOmnifactoryTariff();
    }

    @Override
    public boolean isFreeTransfer() {
        return false;
    }

    @Override
    public boolean isParticipatesInEconomy() {
        return false;
    }

    @Override
    public void updateCargoPrePlayerInteraction() {
        // TODO: Modify demand
    }

    public Map<String, ShipyardData> getShipData() {
        return shipData;
    }

    public Map<String, ShipyardData> getWepData() {
        return wepData;
    }

    public Map<String, ShipyardData> getWingData() {
        return wingData;
    }

    public void setRefit(boolean refit) {
        this.refit = refit;
    }

    public boolean isRefit() {
        return refit;
    }

    protected boolean isDisplayCosts() {
        return displayCosts;
    }

}
