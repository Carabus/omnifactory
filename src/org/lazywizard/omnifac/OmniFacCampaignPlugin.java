package org.lazywizard.omnifac;

import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.*;
import org.lazywizard.omnifac.secondary.AutofactoryInteractionDialog;
import org.lazywizard.omnifac.secondary.Constants;
import org.lazywizard.omnifac.secondary.MarketSearchInteractionDialog;


public class OmniFacCampaignPlugin extends BaseCampaignPlugin {

    @Override
    public String getId() {
        return "OmniFacCampaignPlugin";
    }

    @Override
    public boolean isTransient() {
        return true;
    }

    @Override
    public PluginPick<InteractionDialogPlugin> pickInteractionDialogPlugin(SectorEntityToken interactionTarget) {
        if (interactionTarget.getMarket() != null && interactionTarget.getId().equals("kazeron")) {
            return new PluginPick<InteractionDialogPlugin>(new MarketSearchInteractionDialog("OpenInteractionDialog"), CampaignPlugin.PickPriority.MOD_SPECIFIC);
        }
        if (interactionTarget.getMarket() != null && interactionTarget.getMarket().getName().equals(Constants.STATION_NAME)) {
            return new PluginPick<InteractionDialogPlugin>(new AutofactoryInteractionDialog(), CampaignPlugin.PickPriority.MOD_SPECIFIC);
        }
        return null;
    }
}
