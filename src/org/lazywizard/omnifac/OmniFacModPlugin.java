package org.lazywizard.omnifac;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.*;
import com.fs.starfarer.api.impl.campaign.procgen.themes.RemnantStationFleetManager;
import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import org.apache.log4j.Level;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.CollectionUtils.CollectionFilter;
import org.lazywizard.omnifac.secondary.Constants;
import org.lazywizard.omnifac.secondary.RemnantThemeUtils;

import java.util.*;

import static com.fs.starfarer.api.util.Misc.createStringPicker;
import static org.lazywizard.omnifac.OmniFacSettings.getTargetPlanet;
import static org.lazywizard.omnifac.OmniFacSettings.getTargetSystem;


public class OmniFacModPlugin extends BaseModPlugin {
    private static SectorEntityToken createOmnifactory() {
        SectorAPI sector = Global.getSector();
        SectorEntityToken factory;

        if (!OmniFacSettings.shouldHaveRandomStartingLocation()) {
            String planetId = getTargetPlanet(), systemId = getTargetSystem();
            StarSystemAPI system = sector.getStarSystem(systemId);
            if (system != null) {
                Global.getLogger(OmniFacModPlugin.class).log(Level.INFO,
                    "Omnifactory starting location: orbiting " + planetId + " in " + systemId);
                factory = system.addOrbitalStation(Constants.STATION_ID,
                    system.getEntityById(planetId), 315f,
                    300f, 50f, Constants.STATION_NAME, Constants.STATION_FACTION);
                factory.setCircularOrbitPointingDown(system.getEntityById(planetId), 315f, 300f, 50f);

                // By default the Omnifactory orbits the planet Jangala in the Corvus system
                return factory;
            }

            Global.getLogger(OmniFacModPlugin.class).log(Level.INFO,
                systemId + " not found, using random Omnifactory location");
        }

        // Find a random planet or star that doesn't already have a station
        List<StarSystemAPI> systems = new ArrayList<>(sector.getStarSystems());
        Collections.shuffle(systems);
        for (StarSystemAPI system : systems) {
            CollectionFilter planetFilter = new EmptyOrbitFilter(system);
            List<PlanetAPI> planets = CollectionUtils.filter(
                system.getPlanets(), planetFilter);
            if (!planets.isEmpty()) {
                Collections.shuffle(planets);
                PlanetAPI toOrbit = null;
                for (PlanetAPI planet : planets) {
                    if (OmniFacSettings.shouldHaveRandomStartingLocationCivilized() && planet.getFaction() == null)
                        continue;
                    toOrbit = planet;
                    break;
                }
                if (toOrbit == null)
                    continue;

                Global.getLogger(OmniFacModPlugin.class).log(Level.INFO,
                    "Omnifactory starting location: orbiting "
                        + toOrbit.getName() + " in " + system.getBaseName());

                factory = system.addOrbitalStation(Constants.STATION_ID, toOrbit,
                    (float) (Math.random() * 360f), toOrbit.getRadius() + 150f,
                    50f, Constants.STATION_NAME, Constants.STATION_FACTION);
                factory.setCircularOrbitPointingDown(toOrbit, 315f, toOrbit.getRadius() + 150f, 50f);

                CampaignFleetAPI station = RemnantThemeUtils.addBattlestation(system, toOrbit, createStringPicker("remnant_station2_Standard", 10f));
                int maxFleets = 10;
                RemnantStationFleetManager activeFleets = new RemnantStationFleetManager(
                    station, 1f, 2, maxFleets, 10f, 8, 24);
                system.addScript(activeFleets);

                return factory;
            }
        }

        // No empty planets found? Orbit a random star
        Collections.shuffle(systems);
        for (StarSystemAPI system : systems) {
            if (system.getStar() != null) {
                Global.getLogger(OmniFacModPlugin.class).log(Level.INFO,
                    "Omnifactory starting location: orbiting "
                        + system.getBaseName() + "'s star");
                return system.addOrbitalStation(Constants.STATION_ID,
                    system.getStar(), (float) (Math.random() * 360f),
                    (system.getStar().getRadius() * 1.5f) + 50f, 50f,
                    Constants.STATION_NAME, Constants.STATION_FACTION);
            }
        }

        // In the unlikely situation where every planet's orbit is occupied
        // and all stars in the sector have somehow vanished...
        throw new RuntimeException("Could not find a valid Omnifactory location!");
    }

    @Override
    public void onGameLoad(boolean newGame) {
        Global.getSector().registerPlugin(new OmniFacCampaignPlugin());
    }

    @Override
    public void onApplicationLoad() throws Exception {
        OmniFacSettings.reloadSettings();
    }

    @Override
    public void onEnabled(boolean wasEnabledBefore) {
        if (!wasEnabledBefore) {
            // Support for multiple factories
            for (int x = 1; x <= OmniFacSettings.getNumberOfFactories(); x++) {
                // Set up the station and its market
                SectorEntityToken factory = createOmnifactory();
                String id = Constants.STATION_ID + "-" + x;
                MarketAPI market = Global.getFactory().createMarket(id, Constants.STATION_NAME, 0);
                SharedData.getData().getMarketsWithoutPatrolSpawn().add(id);
                SharedData.getData().getMarketsWithoutTradeFleetSpawn().add(id);

                market.setPrimaryEntity(factory);
                market.setSurveyLevel(MarketAPI.SurveyLevel.FULL);
                market.setFactionId(Constants.STATION_FACTION);
                market.addCondition(Conditions.ABANDONED_STATION);
                market.setBaseSmugglingStabilityValue(0);
                market.getLocationInHyperspace().set(factory.getLocationInHyperspace());

                Global.getSector().getEconomy().addMarket(market);
                factory.setMarket(market);
                factory.setFaction(Constants.STATION_FACTION);

                // Add Omnifactory submarket to station's market
                OmniFacUtil.initOmnifactory(factory);
            }
        }
    }

    private static class EmptyOrbitFilter implements CollectionFilter<SectorEntityToken> {
        final Set<SectorEntityToken> blocked;

        private EmptyOrbitFilter(StarSystemAPI system) {
            blocked = new HashSet<>();
            for (SectorEntityToken station : system.getEntitiesWithTag(Tags.STATION)) {
                OrbitAPI orbit = station.getOrbit();
                if (orbit != null && orbit.getFocus() != null) {
                    blocked.add(station.getOrbit().getFocus());
                }
            }
        }

        @Override
        public boolean accept(SectorEntityToken token) {
            return !blocked.contains(token);
        }
    }
}
